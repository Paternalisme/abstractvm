#########################################
## MAKEFILE for abstractvm
## barbis_j da-sil_l
#########################################

CC  = g++

CPPFLAGS = -Wall -Wextra

RM  = rm -f

NAME = avm

SRC = main.cpp	    \
      Operand.cpp   \
      OpCreator.cpp \
      Parser.cpp

OBJ = $(SRC:.cpp=.o)


all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
