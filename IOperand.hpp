/* 
 * File:   IOperand.hpp
 * Author: da-sil_l
 *
 * Created on 9 février 2015, 14:10
 */

#ifndef IOPERAND_HPP_
#define	IOPERAND_HPP_

#include <iostream>

enum eOperandType {
    INT8 = 1,
    INT16 = 2,
    INT32 = 3,
    FLOAT = 4,
    DOUBLE = 5
};

class IOperand {
public:
    virtual std::string const& toString() const = 0;

    virtual int getPrecision() const = 0;
    virtual eOperandType getType() const = 0;

    virtual IOperand* operator+(const IOperand &rhs) const = 0;
    virtual IOperand* operator-(const IOperand &rhs) const = 0;
    virtual IOperand* operator*(const IOperand &rhs) const = 0;
    virtual IOperand* operator/(const IOperand &rhs) const = 0;
    virtual IOperand* operator%(const IOperand &rhs) const = 0;

    virtual ~IOperand() {}
};


#endif	/* IOPERAND_HPP_ */

