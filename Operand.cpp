/* 
 * File:   Ints.cpp
 * Author: da-sil_l
 * 
 * Created on 9 février 2015, 14:22
 */

#include "Operand.hpp"
#include "OpCreator.hpp"

template<typename T>
Operand<T>::Operand(eOperandType type, T value) : _type(type), _value(value) {
    _maxValues[0] = CHAR_MAX;
    _maxValues[1] = SHRT_MAX;
    _maxValues[2] = INT_MAX;
    _maxValues[3] = FLT_MAX;
    _maxValues[4] = DBL_MAX;

    _minValues[0] = -CHAR_MAX;
    _minValues[1] = -SHRT_MAX;
    _minValues[2] = -INT_MAX;
    _minValues[3] = -FLT_MAX;
    _minValues[4] = -DBL_MAX;
}

template<typename T>
Operand<T>::Operand(const Operand& orig) : _type(orig._type), _value(orig._value),
                                           _maxValues(orig._maxValues),
					   _minValues(orig._minValues) {
}

template<typename T>
Operand<T>::~Operand() {
}

// GETTERS =====================================================================

template<typename T>
int Operand<T>::getPrecision() const {
    return this->_type;
}

template<typename T>
eOperandType Operand<T>::getType() const {
    return this->_type;
}

template<typename T>
eOperandType Operand<T>::mostPrecise(const IOperand* a, const IOperand* b) const {
    if (a->getType() > b->getType()) {
        return a->getType();
    }
    return b->getType();
}

template<typename T>
const std::string& Operand<T>::toString() const {
    std::ostringstream os;
    os << this->_value;
    std::string* stringValue = new std::string(os.str());

    return *stringValue;
}

// OPERATORS ===================================================================

template<typename T>
Operand<T>& Operand<T>::operator=(const Operand<T>& right) {
    if (this == &right)
        return *this;
    this->_type = right._type;
    this->_value = right._value;
    this->_maxValues = right._maxValues;
    this->_minValues = right._minValues;
    return *this;
}

template<typename T>
IOperand* Operand<T>::operator+(const IOperand& rhs) const {
    eOperandType biggestType = mostPrecise(this, &rhs);
    double rValue;
    std::istringstream rValueString(rhs.toString());
    rValueString >> rValue;

    if ((this->_value + rValue) > this->_maxValues[biggestType - 1]) {
        throw std::overflow_error("Overflow");
    }

    double newValue = this->_value + rValue;
    std::ostringstream newValueOstr;
    newValueOstr << std::fixed << newValue;
    std::string newValueStr(newValueOstr.str());
    return OpCreator::createOperand(biggestType, newValueStr);
}

template<>
IOperand* Operand<double>::operator%(const IOperand& rhs) const {
    (void)rhs;
    throw std::runtime_error("Modulo with floating point numbers");
    return NULL;
}

template<>
IOperand* Operand<float>::operator%(const IOperand& rhs) const {
    (void)rhs;
    throw std::runtime_error("Modulo with floating point numbers");
    return NULL;
}

template<typename T>
IOperand* Operand<T>::operator%(const IOperand& rhs) const {
    eOperandType biggestType = mostPrecise(this, &rhs);
    T rValue;
    std::istringstream rValueString(rhs.toString());
    rValueString >> rValue;
    if (rValue == 0) {
        throw std::runtime_error("Divide by zero");
    }
    T newValue = this->_value % rValue;

    std::ostringstream newValueOstr;
    newValueOstr << std::fixed << newValue;
    std::string newValueStr(newValueOstr.str());
    return OpCreator::createOperand(biggestType, newValueStr);

}

template<typename T>
IOperand* Operand<T>::operator*(const IOperand& rhs) const {
    eOperandType biggestType = mostPrecise(this, &rhs);
    double rValue;
    std::istringstream rValueString(rhs.toString());
    rValueString >> rValue;

    if ((this->_value * rValue) > this->_maxValues[biggestType - 1]) {
        throw std::overflow_error("Overflow");
    }

    double newValue = this->_value * rValue;
    std::ostringstream newValueOstr;
    newValueOstr << std::fixed << newValue;
    std::string newValueStr(newValueOstr.str());
    return OpCreator::createOperand(biggestType, newValueStr);
}

template<typename T>
IOperand* Operand<T>::operator-(const IOperand& rhs) const {
    eOperandType biggestType = mostPrecise(this, &rhs);
    double rValue;
    std::istringstream rValueString(rhs.toString());
    rValueString >> rValue;

    if ((this->_value - rValue) < this->_minValues[biggestType - 1]) {
        throw std::underflow_error("Underflow");
    }

    double newValue = this->_value - rValue;
    std::ostringstream newValueOstr;
    newValueOstr << std::fixed << newValue;
    std::string newValueStr(newValueOstr.str());
    return OpCreator::createOperand(biggestType, newValueStr);
}

template<typename T>
IOperand* Operand<T>::operator/(const IOperand& rhs) const {
    eOperandType biggestType = mostPrecise(this, &rhs);
    double rValue;
    std::istringstream rValueString(rhs.toString());
    rValueString >> rValue;

    if (rValue == 0) {
        throw std::runtime_error("Divide by zero");
    }

    double newValue = this->_value / rValue;
    std::ostringstream newValueOstr;
    newValueOstr << std::fixed << newValue;
    std::string newValueStr(newValueOstr.str());
    return OpCreator::createOperand(biggestType, newValueStr);
}

template Operand<double>::Operand(eOperandType, double);
template Operand<float>::Operand(eOperandType, float);
template Operand<int8_t>::Operand(eOperandType, int8_t);
template Operand<int16_t>::Operand(eOperandType, int16_t);
template Operand<int32_t>::Operand(eOperandType, int32_t);
