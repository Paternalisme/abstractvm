/* 
 * File:   OpCreator.hpp
 * Author: da-sil_l
 *
 * Created on 9 février 2015, 15:28
 */

#ifndef OPCREATOR_HPP_
#define	OPCREATOR_HPP_

#include <sstream>
#include <limits>
#include "Operand.hpp"

typedef char int8_t;
typedef short int16_t;
typedef int int32_t;

template <typename T>
class Operand;

class OpCreator {
private:
    static IOperand* createInt8(const std::string& value);
    static IOperand* createInt16(const std::string& value);
    static IOperand* createInt32(const std::string& value);
    static IOperand* createFloat(const std::string& value);
    static IOperand* createDouble(const std::string& value);
public:
    static IOperand* createOperand(eOperandType, const std::string&);

    virtual ~OpCreator();
    OpCreator();
    OpCreator(const OpCreator& orig);
};

#endif	/* !OPCREATOR_HPP_ */

