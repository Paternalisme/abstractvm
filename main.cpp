/* 
 * File:   main.cpp
 * Author: barbis_j
 *
 * Created on February 9, 2015, 2:04 PM
 */

#include <fstream>
#include <stdexcept>
#include "Parser.hpp"

std::string epur_str(const std::string& line) {
    std::istringstream iss(line);
    std::string w, result;

    if (iss >> w) {
        result += w;
    }
    while (iss >> w) {
        result += ' ' + w;
    }
    return result;
}

int standardInputParse() {
    std::string tmp;
    Parser parse;

    getline(std::cin, tmp);
    while (tmp != ";;") {
        tmp = tmp.substr(0, tmp.find(";"));
        if (!(epur_str(tmp)).empty() && (epur_str(tmp)).at(0) != ';')
            parse.getParse(epur_str(tmp));
        if (getline(std::cin, tmp) == 0)
            return -1;
    }
    try {
        parse.doParse();
    } catch (const std::exception& e) {
        std::cout << parse.getResult().str();
        std::cerr << "\"" << parse.getCurrentLine()
                << "\": " << e.what() << std::endl;
        return -1;
    }
    std::cout << parse.getResult().str();
    return 0;
}

int fileParseError(Parser& parse) {
    try {
        parse.doParse();
    } catch (const std::exception& e) {
        std::cout << parse.getResult().str();
        std::cerr << "\"" << parse.getCurrentLine()
                << "\": " << e.what() << std::endl;
        return -1;
    }
    return 0;
}

int fileParse(const std::string& str) {
    std::string line;
    std::fstream file(str.c_str(), std::fstream::in);
    Parser parse;

    while (file.good()) {
        getline(file, line);
        if (line != "exit") {
            if (!(epur_str(line)).empty() && (epur_str(line)).at(0) != ';')
                parse.getParse(epur_str(line));
        } else {
            if (fileParseError(parse) == -1)
                return -1;
            std::cout << parse.getResult().str();
            return 0;
        }
    }
    if (line != "exit") {
        if (fileParseError(parse) == -1)
            return -1;
        std::cout << parse.getResult().str();
        try {
            throw std::runtime_error("Missing exit at the end of file");
        } catch (std::exception& e) {
            std::cerr << e.what() << std::endl;
            return -1;
        }
    }
    return 0;
}

int main(int argc, char** argv) {
    int i;

    i = 1;
    if (argc == 1) {
        standardInputParse();
    } else {
        while (argv[i])
            fileParse(argv[i++]);
    }
    return 0;
}
