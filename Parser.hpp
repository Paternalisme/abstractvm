/* 
 * File:   parser.hpp
 * Author: barbis_j
 *
 * Created on February 9, 2015, 2:01 PM
 */

#ifndef PARSER_HPP
#define	PARSER_HPP

#include <iostream>
#include <map>
#include <vector>
#include <sstream>
#include <stdexcept>
#include "IOperand.hpp"
#include "OpCreator.hpp"

class Parser {
public:
    Parser();
    Parser(const Parser& orig);
    virtual ~Parser();
    Parser& operator=(const Parser& orig);

    void doParse();
    void getParse(const std::string&);
    const std::ostringstream& getResult() const;
    const std::string& getCurrentLine() const;

private:
    std::vector<IOperand *> opStack;
    std::vector<std::string> parseStack;
    std::map<std::string, void (Parser::*)(IOperand *)> opMapArg;
    std::map<std::string, void (Parser::*)()> opMapNoArg;
    std::map<std::string, eOperandType> typeMap;
    std::ostringstream result;
    std::string currentLine;
    bool exitFlag;

    void push(IOperand *);
    void dump();
    void pop();
    void assert(IOperand *);
    void add();
    void sub();
    void mul();
    void div();
    void mod();
    void print();
    void exit();
};

#endif	/* PARSER_HPP */

