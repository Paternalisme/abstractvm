/* 
 * File:   parser.cpp
 * Author: barbis_j
 * 
 * Created on February 9, 2015, 2:01 PM
 */

#include "Parser.hpp"

Parser::Parser() {
    opMapArg["push"] = &Parser::push;
    opMapArg["assert"] = &Parser::assert;
    opMapNoArg["dump"] = &Parser::dump;
    opMapNoArg["pop"] = &Parser::pop;
    opMapNoArg["add"] = &Parser::add;
    opMapNoArg["sub"] = &Parser::sub;
    opMapNoArg["mul"] = &Parser::mul;
    opMapNoArg["div"] = &Parser::div;
    opMapNoArg["mod"] = &Parser::mod;
    opMapNoArg["print"] = &Parser::print;
    opMapNoArg["exit"] = &Parser::exit;
    typeMap["int8"] = INT8;
    typeMap["int16"] = INT16;
    typeMap["int32"] = INT32;
    typeMap["float"] = FLOAT;
    typeMap["double"] = DOUBLE;
    exitFlag = false;
}

Parser::Parser(const Parser& orig) {
    (void) orig;
}

Parser::~Parser() {
}

Parser& Parser::operator=(const Parser& orig) {
    if (this == &orig)
        return *this;
    (void) orig;
    return *this;
}

void Parser::doParse() {
    std::vector<std::string>::iterator it = parseStack.begin();
    while (it != parseStack.end() && !exitFlag) {
        currentLine = *it;
        if ((*it) != "" && (*it).find(" ") != std::string::npos) {
            std::string tmp = (*it).substr(0, (*it).find(" "));
            std::string tmp2 = (*it).substr((*it).find(" ") + 1, (*it).find("(") - ((*it).find(" ") + 1));
            std::string tmp3 = (*it).substr((*it).find("(") + 1, (*it).find(")") - ((*it).find("(") + 1));
            if (opMapArg[tmp] != 0)
                (this->*opMapArg[tmp])(OpCreator::createOperand(typeMap[tmp2], tmp3));
            else
                throw std::invalid_argument("Wrong syntax");
        } else if ((*it) != "" && opMapNoArg[(*it)] != 0) {
            (this->*opMapNoArg[(*it).substr(0, (*it).find(";"))])();
        } else if ((*it) != "") {
            throw std::invalid_argument("Wrong syntax");
        }
        ++it;
    }
    if (!exitFlag) {
        throw std::runtime_error("Missing exit at the end of file");
    }
}

void Parser::getParse(const std::string& toStore) {
    parseStack.push_back(toStore);
}

void Parser::push(IOperand *n) {
    opStack.insert(opStack.begin(), n);
}

void Parser::dump() {
    std::vector<IOperand *>::iterator it = opStack.begin();
    while (it != opStack.end()) {
        result << (*it)->toString() << std::endl;
        ++it;
    }
}

void Parser::add() {
    if (opStack.size() < 2) {
        throw std::runtime_error("Insufficient stack");
    } else {
        IOperand *tmp = *opStack.begin();
        pop();
        IOperand *tmp2 = *opStack.begin();
        pop();
        tmp = *tmp + *tmp2;
        push(tmp);
    }
}

void Parser::assert(IOperand *op) {
    if (opStack.size() < 1) {
        throw std::runtime_error("Insufficient stack");
    } else if (op->toString() == (*opStack.begin())->toString() &&
            op->getType() == (*opStack.begin())->getType()) {
        return;
    } else {
        throw std::runtime_error("Assert failed");
    }
    return;
}

void Parser::div() {
    if (opStack.size() < 2) {
        throw std::runtime_error("Insufficient stack");
    } else {
        IOperand *tmp = *opStack.begin();
        pop();
        IOperand *tmp2 = *opStack.begin();
        pop();
        tmp = *tmp / *tmp2;
        push(tmp);
    }
}

void Parser::exit() {
    exitFlag = true;
}

void Parser::mod() {
    if (opStack.size() < 2) {
        throw std::runtime_error("Insufficient stack");
    } else {
        IOperand *tmp = *opStack.begin();
        pop();
        IOperand *tmp2 = *opStack.begin();
        pop();
        tmp = *tmp % *tmp2;
        push(tmp);
    }
}

void Parser::mul() {
    if (opStack.size() < 2) {
        throw std::runtime_error("Insufficient stack");
    } else {
        IOperand *tmp = *opStack.begin();
        pop();
        IOperand *tmp2 = *opStack.begin();
        pop();
        tmp = *tmp * *tmp2;
        push(tmp);
    }
}

void Parser::pop() {
    if (opStack.size() < 1) {
        throw std::runtime_error("Stack empty");
    } else {
        opStack.erase(opStack.begin());
    }
}

void Parser::print() {
    std::string charAsString;
    if (opStack.size() < 1) {
        throw std::runtime_error("Stack empty");
    } else {
        if ((*opStack.begin())->getType() == INT8) {
            charAsString = ((*opStack.begin())->toString());
            int charAsInt;
            std::istringstream iss(charAsString);
            iss >> charAsInt;
            char charAsChar = charAsInt;
            result << charAsChar;
        } else {
            throw std::invalid_argument("Not a INT8");
        }
    }
}

void Parser::sub() {
    if (opStack.size() < 2) {
        throw std::runtime_error("Insufficient stack");
    } else {
        IOperand *tmp = *opStack.begin();
        pop();
        IOperand *tmp2 = *opStack.begin();
        pop();
        tmp = *tmp - *tmp2;
        push(tmp);
    }
}

const std::ostringstream & Parser::getResult() const {
    return result;
}

const std::string& Parser::getCurrentLine() const {
    return currentLine;
}
