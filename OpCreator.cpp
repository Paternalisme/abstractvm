/* 
 * File:   OpCreator.cpp
 * Author: da-sil_l
 * 
 * Created on 9 février 2015, 15:28
 */

#include "OpCreator.hpp"

OpCreator::OpCreator() {
}

OpCreator::OpCreator(const OpCreator& orig) {
    (void)orig;
}

OpCreator::~OpCreator() {
}

// FCTS ========================================================================

IOperand* OpCreator::createDouble(const std::string& value) {
    double d;
    long double checks;

    std::istringstream(value) >> checks;
    if (checks > std::numeric_limits<double>::max()) {
        throw std::overflow_error("Overflow");
    }
    if (checks < -std::numeric_limits<double>::max()) {
        throw std::underflow_error("Underflow");
    }
    std::istringstream(value) >> d;

    IOperand *ret = new Operand<double>(DOUBLE, d);
    return ret;
}

IOperand* OpCreator::createFloat(const std::string& value) {
    float f;
    long double checks;

    std::istringstream(value) >> checks;
    if (checks > std::numeric_limits<float>::max()) {
        throw std::overflow_error("Overflow");
    }
    if (checks < -std::numeric_limits<float>::max()) {
        throw std::underflow_error("Underflow");
    }
    std::istringstream(value) >> f;
    return new Operand<float>(FLOAT, f);
}

IOperand* OpCreator::createInt16(const std::string& value) {
    int16_t i16;
    long double checks;

    std::istringstream(value) >> checks;
    if (checks > std::numeric_limits<int16_t>::max()) {
        throw std::overflow_error("Overflow");
    }
    if (checks < std::numeric_limits<int16_t>::min()) {
        throw std::underflow_error("Underflow");
    }
    std::istringstream(value) >> i16;
    return new Operand<int16_t>(INT16, i16);
}

IOperand* OpCreator::createInt32(const std::string& value) {
    int32_t i32;
    long double checks;

    std::istringstream(value) >> checks;
    if (checks > std::numeric_limits<int32_t>::max()) {
        throw std::overflow_error("Overflow");
    }
    if (checks < std::numeric_limits<int32_t>::min()) {
        throw std::underflow_error("Underflow");
    }
    std::istringstream(value) >> i32;
    return new Operand<int32_t>(INT32, i32);
}

IOperand* OpCreator::createInt8(const std::string& value) {
    long double checks;

    std::istringstream(value) >> checks;
    if (checks > std::numeric_limits<int8_t>::max()) {
        throw std::overflow_error("Overflow");
    }
    if (checks < std::numeric_limits<int8_t>::min()) {
        throw std::underflow_error("Underflow");
    }
    return new Operand<int16_t>(INT8, static_cast<int16_t>(checks));
}

IOperand* OpCreator::createOperand(eOperandType type, const std::string& value) {
    if (value.empty())
        throw std::invalid_argument("Wrong syntax");
    if (type > 0 && type < 6) {
        typedef IOperand * (*ptrTab)(const std::string&);
        ptrTab tab[] = {&OpCreator::createInt8,
            &OpCreator::createInt16,
            &OpCreator::createInt32,
            &OpCreator::createFloat,
            &OpCreator::createDouble};
        return (*tab[type - 1])(value);
    } else {
        throw std::invalid_argument("Bad operand type");
    }
    return 0;
}
