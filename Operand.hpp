/* 
 * File:   Ints.hpp
 * Author: da-sil_l
 *
 * Created on 9 février 2015, 14:22
 */

#ifndef OPERAND_HPP_
#define	OPERAND_HPP_

#include <stdexcept>
#include <climits>
#include <sstream>
#include <cfloat>
#include "IOperand.hpp"

template <typename T>
class Operand : public IOperand {
private:
    eOperandType _type;
    T _value;
    double _maxValues[5];
    double _minValues[5];
    eOperandType mostPrecise(const IOperand* a, const IOperand* b) const;
public:
    Operand(const eOperandType, const T);
    Operand(const Operand& orig);
    virtual ~Operand();

    virtual int getPrecision() const;
    virtual eOperandType getType() const;
    virtual const std::string& toString() const;

    virtual IOperand* operator%(const IOperand& rhs) const;
    virtual IOperand* operator*(const IOperand& rhs) const;
    virtual IOperand* operator+(const IOperand& rhs) const;
    virtual IOperand* operator-(const IOperand& rhs) const;
    virtual IOperand* operator/(const IOperand& rhs) const;

    Operand& operator=(const Operand& right);
};

#endif	/* !OPERAND_HPP_ */

